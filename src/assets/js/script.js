 (function($) {
   let calcFont = 0;
   let fontSize = 0;
   let w = $(window);
   function setFontSize(w) {
     if(w.width() < 480) {
       calcFont = w.width() / 480;
       fontSize = 20  * calcFont;
       $('html').css({'font-size': fontSize + 'px'});
     } else {
       $('html').css({'font-size': '20px'});
     }
   }
   setFontSize($(window));
   $(window).resize(function() {
     setFontSize($(this));
   });

   // Comment 상태 변화 버튼(수정/삭제)
  $(document).on('click', '.btn-change-status', function() {
    $(this).parent().find('.status-change-button-wrap').toggleClass('on');
    return false;
  });
 })(jQuery);
 

function openNav() {
  document.getElementById("slide-nav").style.width = "100%";
}

function closeNav() {
  document.getElementById("slide-nav").style.width = "0";
}

$(document).ready(function(){
  
  var acc = document.getElementsByClassName("accordion");
  var i;
  
  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.display === "flex") {
        panel.style.display = "none";
      } else {
        panel.style.display = "flex";
      }
    });
  }
} )

//탭배너
$(document).ready(function(){
	
	$('.tab-banner .item').click(function(){
		var tab_id = $(this).attr('data-tab');

		$(this).siblings('.tab-banner .item').removeClass('current');
		$(this).closest('.tab-container').find('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})
  let nav = document.querySelectorAll('nav.scroll-nav');

  if (nav) {
    let navLinks = document.querySelectorAll('nav a')
    let activeLink = document.querySelector('.active')
    activeLink.scrollIntoView({ behavior: "smooth", inline: "center" })

    navLinks.forEach( function(link) {
      link.addEventListener("click", (event) => {
        navLinks.forEach( function(link) {
          link.classList.remove('active')
        })
        link.classList.add('active')
        link.scrollIntoView({ behavior: "smooth", inline: "center" })
      })
    })
  }
});

//모달창
function modalClose() {
  $('.modal .close-btn,.login-close-btn').click(function() {
    $(this).parents('.modal-wrap').removeClass('is-visible');
    $('body').css("overflow", "auto");
  });
}
// function modalClose() {
//   $('.modal .login-close-btn').click(function() {
//     $(this).parents('.modal-wrap').removeClass('is-visible');
//     $('body').css("overflow", "auto");
//   });
// }

function modalOpen(target) {
  $('.' + target).addClass('is-visible');
  $('body').css("overflow", "hidden");
}
//} );

//}

jQuery(document).ready(function() {
	var bodyOffset = jQuery('html').offset();
	jQuery(window).scroll(function() {
		if (jQuery(document).scrollTop() > bodyOffset.top) {
			jQuery('.floating-menu').addClass('show');
		} else {
			jQuery('.floating-menu').removeClass('show');
		}
	});
});

$(document).ready(function(){ $(window).scrollTop(0); });

let removeToast;

function toast(string) {
    const toast = document.getElementById("toast");

    toast.classList.contains("reveal") ?
        (clearTimeout(removeToast), removeToast = setTimeout(function () {
            document.getElementById("toast").classList.remove("reveal")
        }, 1000)) :
        removeToast = setTimeout(function () {
            document.getElementById("toast").classList.remove("reveal")
        }, 1000)
    toast.classList.add("reveal"),
        toast.innerText = string
}